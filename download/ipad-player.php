<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Downloading iPad NetTv Player</h1>

			<p>1. Access the Safari web browser on your iPad, and type in the web address: <a href="http://ipad.nettvplus.com/">http://ipad.nettvplus.com/</a></p>

			<p>When you type a web address, click on GO button, to go to the home page of iNetTv player.iPad NetTv Player</p>

			<img src="/images/ipad_1.jpg">

			<p>2. Access the NetTv Plus homepage. You will receive instructions on how to add an application to the desktop, after that start the app from the main menu.iPad NetTv Player</p>

			<img src="/images/ipad_2.jpg">

			<p>3. When you start the NetTv Plus application from the home screen you will get SMP number, which is required for the activation. To activate your app contact our call center, by calling the listed phone numbers. When the call center operator verifies that the device is activated, press the circular arrow button in the form, or just restart the application. iPad NetTv Player</p>

			<img src="/images/ipad_3.jpg">

			<p>4. To start watching TV program, click on TV icon, from the main menu, and then select one of the following categories of TV channels (all, children's, sports, video, music, film, and XXX).iPad NetTv Player</p>

			<img src="/images/ipad_4.jpg">

			<p>5. After selecting a category from the list, select a desired channel by clicking on its name and wait to be loaded into the small screen on the right.iPad NetTv Player</p>

			<img src="/images/ipad_5.jpg">

			<p>Clicking on the button <img src="/images/ipad_a.jpg" alt="Full Screen mode"> app is entering/leaving the Full Screen mode.</p>

			<p>6. If the subscription expires, you can see the identification number of the device, by going to the main menu, click on "Options" and select "Info Box". Here you can see the subscription expiration date.iPad NetTv Player</p>

			<img src="/images/ipad_6.jpg">

			<p>7. In the "Options" menu, you can configure the application to meet your needs.</p>

			<p><img src="/images/ipad_b.jpg" alt="iPad NetTv Player"> Information about of subscription expiration and the SMP number of device.</p>

			<p><img src="/images/ipad_c.jpg" alt="iPad NetTv Player"> Sets up backup memory that delays start of TV channel, which improves image stability.</p>

			<p><img src="/images/ipad_d.jpg" alt="iPad NetTv Player"> Change the language in which you use the application.</p>

			<p><img src="/images/ipad_e.jpg" alt="iPad NetTv Player"> Choose the picture quality in relation to the speed of your internet connection.</p>

			<p><img src="/images/ipad_f.jpg" alt="iPad NetTv Player"> Selection option "-6" will adjust playback with a delay of 6 hours, while selecting "0" returns to the program that is broadcast live.</p>

			<p><img src="/images/ipad_g.jpg" alt="iPad NetTv Player"> Tests the speed of the internet connection.</p>

			<p><img src="/images/ipad_h.jpg" alt="iPad NetTv Player"> Entering the correct code allows running adult TV channels.</p>

			<p><img src="/images/ipad_i.jpg" alt="iPad NetTv Player"> Reset application.</p>

			<p>8. In addition to watching TV channels, with the NetTv Plus application you can listen to radio stations, read news and books, and surf the web.</p>

			<img src="/images/ipad_bottom.jpg">

		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>