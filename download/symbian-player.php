<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Downloading Symbian NetTv Player</h1>
			<p>NetTv Symbian player is a new NetTv software, which allows you to turn your Symbian phone into a television box and follow your favorite TV channels live. In order to use NetTv Symbian, you need to have the Nokia Series 60 or later version of this cell phone.</p>

			<p>The next few steps will explain in detail how to activate NetTv Symbian.</p>

			<p>1. Access the web broser on your Symbian phone. Next, in the field provided to enter the web address, please type: <a href="http://symbian.nettvplus.com">http://symbian.nettvplus.com</a></p>

			<img src="/images/symbian_1.jpg" alt="NetTV plus nettv symbian">

			<p>2. After you have typed in the web address, click on the GO button in order to access NetTv Symbian player Home.</p>

			<img src="/images/symbian_2.jpg" alt="NetTV plus nettv symbian">

			<p>3. Select SETTINGS, then ACCESS CODE. Here please type the 8-digit code that our operators have previously sent to your email address. Click the VERIFY button.</p>

			<img src="/images/symbian_3.jpg" alt="NetTV plus nettv symbian">

			<p>4. After verifying, a list of channels will appear on your TV screen. Click on the name of the channel you want and wait for loading to finish.</p>

			<img src="/images/symbian_4.jpg" alt="NetTV plus nettv symbian">

			<p>5. Once the channel has been loaded, you can start enjoying your favorite TV programs.</p>

			<img src="/images/symbian_5.jpg" alt="NetTV plus nettv symbian">

			<p>6. Selecting SETTINGS from the Start Menu enables you to customize the player to your own needs. Selecting ACCESS CODE sets and modifies your access code. Selecting LANGUAGE allows you to change the language. Selection -6 CHANNELS is designed for American and Canadian users to play the channels with a 6-hour delay. Selecting XXX CODE enables the parental control option and placing adult channels under an access code.</p>

			<img src="/images/symbian_6.jpg" alt="NetTV plus nettv symbian">

		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>