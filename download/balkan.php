<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Download Balkan TV | Ex-Yu TV Software</h1>
			<div class="full_width">
				<p>We offer a wide range of download options to get you ready for the show. Select which program you want then select your operating system or device.</p>
			</div>
			<div class="full_width">
				<div class="accordion-wrapper"><a href="javascript:void(0)" class="accordion-title orange"><span>PC player 4<img src="/images/download_windows.png"> <img src="/images/download_apple.png"> <img src="/images/download_linux.png"></span></a>
					<div class="accordion-content">
						<p>NetTV plus PC player is a multimedia software which allows You to watch live TV channels via Your lap top or desktop computer, wherever You are. All You need to do is to download the player from our website, enter Your key, and You can start watching your favourite TV channels.</p>
						<img src="/images/download_player_1.png">
						<img src="/images/download_player_2.png">
						<ul class="list tick">
							<li>Over 200 EX-YU tv channels</li>
							<li>HD channels</li>
							<li>Creating a list of favourite channels</li>
							<li>The most popular radio stations</li>
							<li>Automatic updates</li>
							<li>Possibliity of recording and reproducing the recorded content</li>
							<li>Changing aspects in relation to the monitor - 4:3, 16:9, 16:10, 5:4</li>
							<li>Channels grouped by categories: Movie, Kids, Sports, Music, XXX</li>
						</ul>
						<a href="http://desktop.nettvplus.com/static/resources/nettvplus/player/setup/win/nettv_player_pc_v400.exe" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_windows_inverted.png"></a>
						<a href="http://desktop.nettvplus.com/static/resources/nettvplus/player/setup/mac/nettv_player_mac_v400.dmg" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_apple_inverted.png"></a>
						<a href="http://desktop.nettvplus.com/static/resources/nettvplus/player/setup/lin32/nettv_player_linux_v4.tar.gz" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_linux_inverted.png"></a>
					</div>
				</div>
				<div class="accordion-wrapper"><a href="javascript:void(0)" class="accordion-title orange"><span>Flash player <img src="/images/download_windows.png"> <img src="/images/download_apple.png"> <img src="/images/download_linux.png"> <img src="/images/download_playstation.png"></span></a>
					<div class="accordion-content">
						<p>NetTv Plus Flash player is an application that enables you to watch your favorite TV channels on computers with Windows, Mac and Linux operating systems, as well as the SonyPlayStation. Flash player can run on almost all devices that support Adobe Flash Player. Player does not require any installation, and to start it, you just have to have installed Adobe Flash Player. Simply go to this web page http://player.nettvplus.com, enter your eight-digit code, and you can start watching TV.</p>
						<img src="/images/download_flash_4.png">
						<ul class="list tick">
							<li>Over 200 EX-YU TV channels</li>
							<li>Easy to start and use</li>
							<li>No download or installation required</li>
							<li>EPG - Electronic program guide</li>
							<li>Can be used on Windows, Mac and Linux OS , as well as the Sony PlayStation</li>
							<li>Channels grouped by categories: Film, Kids, Sports, Music, XXX</li>
							<li>Automatic updates</li>
						</ul>
						<a href="/download/flash.php" class="button large black rnd8">Download</a>					
					</div>
				</div>
				<div class="accordion-wrapper"><a href="javascript:void(0)" class="accordion-title orange"><span>Smartphone / Tablet player <img src="/images/download_iphone.png"> <img src="/images/download_ipad.png"> <img src="/images/download_android.png"> <img src="/images/download_symbian.png"></span></a>
					<div class="accordion-content">
						<p>NetTV Plus has made applications for mobile phone. Now You can have all TV channels on Your fingertips!</p>
						<p>NetTV plus player ipad TV channels are available whrever there is internet connection - WiFi, 3G, 4G. TV channels are broadcast in real time, and the users are allowed to delay watching by using the option -6 h.</p>
						<p>Each channel has EPG in addition, so a user could be aware of which show they are currently following, and what is next on the program.</p>
						<img src="/images/download_smartphone.png">
						<ul class="list tick">
							<li>Multimedial software</li>
							<li>Over 200 EX YU, foreign TV channels and adults TV channels</li>
							<li>Multilingual applications</li>
							<li>Possibility of connecting to any TV or monitor</li>
							<li>Quality image in h.264 format</li>
							<li>Easy to use</li>
							<li>Possibility of watching TV channels in real and delayed (-6h) time</li>
							<li>EPG program announcement</li>
							<li>Possibility of pausing and resuming TV channels, in order to not miss the important details</li>
							<li>Perfect to use on places where You have never watched Your favourite match, movie or show before</li>
						</ul>
						<a href="/download/iphone-player.php" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_iphone_inverted.png"></a>
						<a href="/download/ipad-player.php" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_ipad_inverted.png"></a>
						<a href="/download/android-player.php" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_android_inverted.png"></a>
						<a href="/download/symbian-player.php" class="button large black rnd8">Download<img class="download-button-img" src="/images/download_symbian.png"></a>
					</div>
				</div>
				<div class="accordion-wrapper"><a href="javascript:void(0)" class="accordion-title orange"><span>Samsung Smart TV <img src="/images/download_samsung.png"></span></a>
					<div class="accordion-content">
						<p>Samsung Smart TV is the next generation of television, which in addition to watching the television channels, enables interactive use of the Internet. This means that users can watch TV and surf the Internet on a single device.</p>
						<img src="/images/download_samsung_smart_tv.png">
						<p>Due to high demand, the Net TV Plus has developed an application for Samsung Smart TVs for all its users, with which you can watch all the TV channels that are included in our offer.</p>
						<p>Currently, Net TV Plus Samsung Smart TV application is in beta and is still being tested. Feel free to let us know if you have any problems at all.</p>
						<a href="/download/samsung.php" class="button large black rnd8">Download</a>
					</div>
				</div>
			</div>
		</section>	
<!-- ################################################################################################ -->
<div class="clear"></div>
</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>