<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Downloading Android NetTv player</h1>

			<p>NetTv Android player is a new NetTv Plus software, which allows you to turn your Andriod phone into a television box and follow your favourite TV channels live. In order to use NetTv Android player, you need to have the Android 2.2 or newer version of this popular OS. Also, this service only works on Wi-Fi connections.</p>

			<img src="/images/android_1.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">

			<p>The next few steps will explain in detail how to activate your NetTv Andriod.</p>

			<p>1. Access Play Store application (formerly Android Market) and  type "NetTv Player" in search field, click on the icon NetTV Player and start the installation. Player can also be downloaded directly from the following link: <a href="http://download.citymediaservices.com/android/nettvplayer.apk">http://download.citymediaservices.com/android/nettvplayer.apk</a> (for this it is necessary to turn on the "Unknown sources" on the device).</p>

			<img src="/images/android_2.png" alt="NetTv Plus nettv android  NetTv Plus nettv android"> <img src="/images/android_3.png" alt="NetTv Plus nettv android  NetTv Plus nettv android"> <img src="/images/android_4.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">

			<p>2. After the first run of the application, the device is not yet active. Activation can be done in two ways:</p>

			<ul class="list tick">
				<li>The first is to click "Auto activate", properly fill out your information in the form and click OK. Then the request will be sent automatically and quickly after that you will be contacted by our call center.</li>
				<li>Another way is to contact our call center directly by phone.</li>
			</ul>

			<img src="/images/android_5.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">

			<p>3. After activation of player, from the initial menu, select the category of channels and desired TV channel. Wait for the channel to load. If you do not have Vitamio plugin installed on your phone, the installation will start automatically. All you need to do is to confirm installation. After installation, restart the player.</p>

			<img src="/images/android_6.png" alt="NetTv Plus nettv android  NetTv Plus nettv android"> <img src="/images/android_7.png" alt="NetTv Plus nettv android  NetTv Plus nettv android"> <img src="/images/android_8.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">

			<p>4. NetTv Plus Android application has 4 basic options:</p>

			<p><img src="/images/android_a.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">TV channels: From here you can first chose channel category, and than TV channel that you would like to watch.</p>

			<p><img src="/images/android_b.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">News: Here you can read news from ex Yugoslavian countries</p>

			<p><img src="/images/android_c.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">Options: Customize the application as you like. Here you can configure language,lighting, orientation of the device, the ratio v / s quality, buffer, pre-buffer time.</p>

			<p><img src="/images/android_d.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">Key: Here you can see ID number of your device, and our phone numbers.</p>

			<img src="/images/android_9.png" alt="NetTv Plus nettv android  NetTv Plus nettv android">
		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>