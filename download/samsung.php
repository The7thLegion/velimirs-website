<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Downloading Samsung Smart TV</h1>

			<p>Installation instructions for NetTV Plus test application for Samsung Smart TV’s that are manufactured in 2011 and 2012.</p>
			<p>Important notice! Please do not update your SMART HUB, in order to prevent to stop functioning our test application for Samsung Smart TV.</p>
			<p>If you have older version of this TV, please click here: Samsung Smart 2010</p>
			<p>1.  Smart Hub button on the remote controle.</p>
			<p>2. Click Login, or press the red button on the remote control.</p>

			<img src="/images/install_samsung_1.jpg" alt="SMART HUB Login">

			<p>3. Enter the user name (develop) and password (123456) and select Login.</p>

			<img src="/images/install_samsung_2.jpg" alt="Login">

			<p>If the login is successful, the develop icon is displayed at the bottom-left corner of the screen.</p>

			<img src="/images/install_samsung_3.jpg" alt="Develop Icon">

			<p>4. Click Settings to enter settings or press the blue button on the remote control to enter the setup.</p>
			<p>A new option, Development, should appear at the end of the Settings menu.</p>

			<img src="/images/install_samsung_4.jpg" alt="Development">

			<p>5. Select Development and choose the Setting Server IP option.</p>

			<img src="/images/install_samsung_5.jpg" alt="Setting Server IP">

			<p>6. In the boxes, enter the IP address (84.20.255.9)  of the web server that contains the packaged NetTV Plus application and press the Return button on the remote control.</p>

			<img src="/images/install_samsung_6.jpg" alt="Server IP">

			<p>7. From the Development menu in Settings, select User Application Synchronization.</p>

			<img src="/images/install_samsung_7.jpg" alt="Synchronization">

			<p>8. The TV indicates that it is installing a new service, and displays the name of NetTV Plus application. After the installation is complete, select Complete and press the Return key on the remote control two times to exit settings.</p>

			<img src="/images/install_samsung_8.jpg" alt="App Sync">

			<p>9. Reboot your Samsung Smart TV, that is, turn it off and on again.</p>
			<p>If the installation was successful, a NetTV Plus application is available in Smart Hub. The application icon contains the text USER in the lower right corner on a red background</p>
			<p>Note: Once you have installed the NetTV Plus application, you do not have to re-login to develop account or to re-install the application each time.</p>

			<img src="/images/install_samsung_9.jpg" alt="App Installed">

			<p>Starting NetTV Plus application</p>
			<p>To start NetTV Plus application</p>
			<p>1. Select NetTV Plus application and press the Enter key on the remote controle and wait while the application is loading.</p>
			<p>2. Press the Return key on the remote control to return to the Smart Hub.</p>
			<p>3. Press the Exit button to exit the Samsung Smart TV Smart Hub.</p>
			<p>Deleting NetTV Plus application</p>
			<p>To delete NetTV Plus application:</p>
			<p>1. Turn on the Samsung Smart TV Smart Hub</p>
			<p>2. Log on to (develop) account.</p>
			<p>3. Press the yellow button on the remote control to access the Edit Mode.</p>
			<p>4. Select NetTV application and press the Enter key on the remote control to delete it.</p>
		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>