<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Downloading Flash Player</h1>
			<div class="introBlurb">
				<a href="http://get.adobe.com/flashplayer/">Download Adobe Flash Player</a>
			</div>
			<p>NetTv Plus Flash player is an application that enables you to watch ex YU TV channels on computers with Windows, Mac and Linux operating systems, as well as on the Sony PlayStation. The only condition for player to work is to have installed Adobe Flash player (if you do not have it installed, at the bottom of the instructions you canfind a link to install). Launching the player is very simple, for a more detailed explanation, read the next few steps.</p>

			<p>1. Start the player by typing : <a href="http://player.nettvplus.com/">PLAYER.NETTVPLUS.COM</a> in web browser or simply click the following link: <a href="http://player.nettvplus.com">http://player.nettvplus.com</a>.</p>

			<p>2. When the page loads you will see the <span class="textBold">BRW</span> number. That number you will tell to our operators from Call Center by calling one of numbers from the screen.NetTv Plus flash player</p>

			<img src="/images/flash_player_2.jpg">

			<p>3. When the operator verifies that the device is activated, click on the circular arrow. Then from the available options, select the desired language.NetTv Plus flash player</p>

			<img src="/images/flash_player_3.jpg">

			<p>4. To start watching a TV channels, please choose option <span class="textBold">KANALI</span> from the main menu.</p>

			<img src="/images/flash_player_4.jpg">

			<p>5. Then select one of the following categories of TV channels by clicking on appropriate icon. NetTv Plus flash player</p>

			<img src="/images/flash_player_5.jpg">

			<p>6. After selecting a category, choose desired TV channel from the list. The channel will run, simply by double-clicking on the channel name. NetTv Plus flash player</p>

			<img src="/images/flash_player_6.jpg">

			<p>7. If the subscription expires, the identification number of the device, at any time, you can see by going to the main menu, click on "<span class="textBold">Options</span>" and select "<span class="textBold">Info Box</span>". Here you can see the subscription expiration date.NetTv Plus flash player</p>

			<img src="/images/flash_player_7.jpg">
		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>