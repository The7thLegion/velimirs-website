<?php include 'header.php'; ?>
<!-- ################################################################################################ -->
<?php
if (isset($_GET['success'])) {
echo '<div class="wrapper row1"><div id="intro" style="padding: 0px;"><div class="alert-msg rnd8 success" style="margin: 30px 0;">Thank you, we will attempt to contact you shortly<a class="close" href="#">X</a></div></div></div>';
}

if (isset($_GET['comingSoon'])) {
echo '<div class="wrapper row1"><div id="intro" style="padding: 0px;"><div class="alert-msg rnd8 warning" style="margin: 30px 0;">Albanian channels coming soon, thanks for your patience while we get it working for you<a class="close" href="#">X</a></div></div></div>';	
}
?>
<div class="wrapper row1 fixedIntroHeight">
	<!-- ################################################################################################ -->
	<div id="intro" style="position: relative;">
		<div class="boxholder bannerBox" banner_number="1" style="display: block;">
			<a href="https://en.nettvplus.com/" target="_blank"><img src="/images/banner-1.jpg" alt="Net TV Plus"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="2">
			<a href="https://en.nettvplus.com/" target="_blank"><img src="/images/banner-2.jpg" alt="Net TV Plus"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="3">
			<a href="http://www.tvrussialive.net/" target="_blank"><img src="/images/banner-3.jpg" alt="Russia"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="4">
			<a href="http://www.tvturklive.com/" target="_blank"><img src="/images/banner-4.jpg" alt="Turk"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="5">
			<a href="http://www.tvpolskalive.com/" target="_blank"><img src="/images/banner-5.jpg" alt="Polska"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="6">
			<a href="http://www.tvitalialive.com/" target="_blank">
				<img src="/images/banner-6.jpg" alt="Italia">
			</a>
		</div>
		<div class="banner_select">
			<ul>
				<li class="select_banner_button selected_banner" banner_number="1"></li>
				<li class="select_banner_button" banner_number="2"></li>
				<li class="select_banner_button" banner_number="3"></li>
				<li class="select_banner_button" banner_number="4"></li>
				<li class="select_banner_button" banner_number="5"></li>
				<li class="select_banner_button" banner_number="6"></li>
			</ul>
		</div>
	</div>
	<!-- ################################################################################################ -->
	<div class="clear"></div>
</div>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<div id="homepage" class="clear">
			<div class="three_fifth first">
				<h1 class="emphasise">Global NetTV</h1>

				<p>Global NetTV is an Australian company that offers you a wide range of products from the IPTV industry.</p>
				<p>Our main goal is providing different cultures with the best quality channels from their homeland.</p>
				<p>Old fashioned Satellite dishes, large receivers or DVB-S cards are not needed anymore. All you need is an Internet connection in your home and we will provide you with a small, sleek and hard to see Albis/Motorola receiver for your living room.</p>
				<p>We offer LIVE TV:</p>
				<ul class="list tick">
					<li>Over 80 channels from Russia on our TV Russia Live platform</li>
					<li>Over 60 channels from Poland on our TV Polska Live platform</li>
					<li>Over 50 channels in Albanian language on our IPTV Iliria platform</li>
					<li>Over 30 channels from Spain on our TV Espana Live platform</li>
					<li>Over 50 channels from Italy on our TV Italia Live platform</li>
					<li>Over 70 channels from Turkey on our TV Turk Live platform</li>
					<li>And over 200 channels from Bosnia, Croatia, Serbia, Macedonia, Slovenia and Montenegro on our Net TV Plus multimedia platform</li>
				</ul>
			</div>
			<div class="two_fifth">
				<img src="images/what_is_tv.png" alt="">
			</div>
		</div>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>

<?php include 'footer.php'; ?>
