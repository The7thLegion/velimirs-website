<?php

function died($field) {

	$name = urlencode($_POST['ftname']);
	$emailFrom = urlencode($_POST['ftemail']);
	$contactNumber = urlencode($_POST['ftcontactNumber']);
	$comments = urlencode($_POST['ftcomments']);

	header("Location:".$_SERVER['HTTP_REFERER']."?ftfield=".$field."&ftname=".$name."&ftemail=".$emailFrom."&ftcontactNumber=".$contactNumber."&ftcomments=".$comments."#freeTrial");
	die();
}

function clean_string($string) {
	$bad = array("content-type","bcc:","to:","cc:","href");
	return str_replace($bad,"",$string);
}

// EDIT THE 2 LINES BELOW AS REQUIRED
$email_to = "office@globalnettv.com.au";
// Debugging email address.
// $email_to = "vandevelde.jacob@gmail.com";
$email_subject = "Global Net TV: FREE TRIAL REQUEST";

$name = $_POST['ftname'];
$emailFrom = $_POST['ftemail'];
$contactNumber = $_POST['ftcontactNumber'];

$Serbia = isset($_POST['Serbia']) && $_POST['Serbia']  ? 'Serbia | ' : '';
$Bosnia = isset($_POST['Bosnia']) && $_POST['Bosnia']  ? 'Bosnia | ' : '';
$Croatia = isset($_POST['Croatia']) && $_POST['Croatia']  ? 'Croatia | ' : '';
$Macedonia = isset($_POST['Macedonia']) && $_POST['Macedonia']  ? 'Macedonia | ' : '';
$Montenegro = isset($_POST['Montenegro']) && $_POST['Montenegro']  ? 'Montenegro | ' : '';
$Slovenia = isset($_POST['Slovenia']) && $_POST['Slovenia']  ? 'Slovenia | ' : '';
$Russia = isset($_POST['Russia']) && $_POST['Russia']  ? 'Russia | ' : '';
$Albania = isset($_POST['Albania']) && $_POST['Albania']  ? 'Albania | ' : '';
$Polska = isset($_POST['Polska']) && $_POST['Polska']  ? 'Polska | ' : '';
$Italia = isset($_POST['Italia']) && $_POST['Italia']  ? 'Italia | ' : '';
$Turk = isset($_POST['Turk']) && $_POST['Turk']  ? 'Turk | ' : '';
$Spain = isset($_POST['Spain']) && $_POST['Spain']  ? 'Spain | ' : '';

$listOfCountries = '| ' . $Serbia . $Bosnia . $Croatia . $Macedonia . $Montenegro . $Slovenia . $Russia . $Albania . $Polska . $Italia . $Turk . $Spain;
$countries = $_POST['ftcountries'];
$comments = $_POST['ftcomments'];

$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
$number_exp = "/^[0-9 +]+$/";

if (!preg_match($email_exp,$emailFrom)) {
	died('ftemail');
}

if (!preg_match($number_exp,$contactNumber)) { 
	died('ftcontactNumber');
} 

$email_message = '<html><body>';

$email_message .= "<h1>FREE TRIAL REQUEST</h1><hr /><table style='vertical-align:middle;'>";

$email_message .= "Form information<hr /><table style='vertical-align:middle;'>";

$email_message .= "<tr style='height: 30px;'><td><b>Name:</b></td><td>".clean_string($name)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td><b>Email:</b></td><td>".clean_string($emailFrom)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td style='padding-right: 15px;'><b>Contact Number:</b></td><td>".clean_string($contactNumber)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td style='padding-right: 15px;'><b>Countries interested in:</b></td><td>".clean_string($listOfCountries)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td><b>Comments:</b></td><td>".clean_string($comments)."</td></tr>";

$email_message .= '</table></html></body>';

$headers = "From: " . strip_tags($_POST['ftemail']) . "\r\n";
$headers .= "Reply-To: ". strip_tags($_POST['ftemail']) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

@mail($email_to, $email_subject, $email_message, $headers);  
header("Location: index.php?success");
die();
