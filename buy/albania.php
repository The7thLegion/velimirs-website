<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Buy Balkan TV | Ex-Yu TV Products</h1>			
			<div class="tab-wrapper clear">
				<div class="tab-container">
					<!-- Tab Content -->
					<div id="tab-1" class="tab-content clear">
						<h1 class="emphasise">Motorola VIP 1003 Box + Basic Subscription</h1>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + BASIC subscription for 3 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$150,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$81.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$286.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="KC8NC9U3Q3YMC">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + BASIC subscription for 6 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$150,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$162.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$367.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="E33HJMZVJ4AL4">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + BASIC subscription for 12 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$150,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$324.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$529.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="5ADYNEZUSYS36">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<h1 class="emphasise">Motorola VIP 1003 Box + Premium Subscription</h1>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + PREMIUM subscription for 3 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$150,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$99.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$304.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="9AVGS99U74MWL">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + PREMIUM subscription for 6 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$162,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$198.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$403.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="64J5XGADPYVDW">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + PREMIUM subscription for 12 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Motorola VIP 1003 Box</td>
										<td>$150,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$396.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$601.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="ZAQPUC3FGM3TA">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<h1 class="emphasise">BOX Subscription Renew</h1>
						</div>
						<div class="full_width">
							<table class="center-content">
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>BASIC Subscription for 3 months</td>
										<td>3 x $27.00 AUD</td>
										<td class="textBold">$81.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="VTW9DHD4XVFRW">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>BASIC Subscription for 6 months</td>
										<td>6 x $27.00 AUD</td>
										<td class="textBold">$162.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="PEPSQ8RT5HC3Y">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>BASIC Subscription for 12 months</td>
										<td>12 x $27.00 AUD</td>
										<td class="textBold">$324.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="VAE6YGHS6X9WY">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>
								<tbody>
									<tr class="light center-content">
										<td>PREMIUM Subscription for 3 months</td>
										<td>3 x $33.00 AUD</td>
										<td class="textBold">$99.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="878DB66JSWUX6">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>PREMIUM Subscription for 6 months</td>
										<td>6 x $33.00 AUD</td>
										<td class="textBold">$198.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="TJ9KZ4BNHLZQY">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>PREMIUM Subscription for 12 months</td>
										<td>12 x $33.00 AUD</td>
										<td class="textBold">$396.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="5PHT9WJRX3X8U">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>						
					</div>
				</div>
			</div>
		</section>
		<section class="clear">
			<div class="full_width">
				<h1>What is ILIRIA IPTV</h1>
				<p>Dalex Media Group S.A. has created special service Iliria IPTV, which enables users to enjoy TV and radio channels on TV, PC or phone, and use numerous interactive services via Internet.</p>
				<p>Iliria IPTV is a new way of viewing TV programs with a specialized Iliria browser which searches only for currently popular and interesting multimedia content using filters.</p>
				<p>Iliria browser automatically searches and sorts multimedia content. As well as radio and TV channels, it searches newspapers, books, games, music videos and films!</p>
				<p>With the specialized Iliria Set top box which includes Iliria browser, numerous home and foreign TV channels will be available on your TV. From news documentaries to sports shows and movies. The offer also includes the radio as an irreplaceable part of the home atmosphere.</p>
				<p>The most important features of Iliria Set top box are a digital picture of high definition and access to HD TV channels.</p>
				<img src="/images/albania_channel_1.png">
				<img src="/images/albania_channel_2.png">
				<img src="/images/albania_channel_3.png">
				<img src="/images/albania_channel_4.png">
			</div>
		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>