<?php 
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/header.php";
include_once($path);
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Buy Balkan TV | Ex-Yu TV Products</h1>			
			<div class="tab-wrapper clear">
				<ul class="tab-nav clear">
					<li><a class="productMenuLink" href="#tab-1"><img src="/images/selected_nettv_plus_box.png" alt="Net TV plus box"></a></li>
					<li><a class="productMenuLink" href="#tab-2"><img src="/images/pc_mac_player.png" alt="PC/Mac Player"></a></li>
					<li><a class="productMenuLink" href="#tab-3"><img src="/images/smartphone.png" alt="Smartphone"></a></li>
					<li><a class="productMenuLink" href="#tab-4"><img src="/images/renew_box.png" alt="Renew Box"></a></li>
					<li><a class="productMenuLink" href="#tab-5"><img src="/images/samsung_smart.png" alt="Samsung Smart"></a></li>
				</ul>
				<div class="tab-container">
					<!-- Tab Content -->
					<div id="tab-1" class="tab-content clear">
						<h1 class="emphasise">Net TV Plus Box</h1>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + subscription for 3 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>IPTV Set Top Box</td>
										<td>$165,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$75.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$295.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="WR42AMKCJLPYW">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + subscription for 6 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>IPTV Set Top Box</td>
										<td>$165,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$150.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$370.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="8LAHKTBEXGZBY">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="three_quarter first">
							<table class="center-content">
								<thead>
									<tr>
										<th colspan="2">Box + subscription for 12 months</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>IPTV Set Top Box</td>
										<td>$165,00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Subscription price</td>
										<td>$300.00 AUD</td>
									</tr>
									<tr class="light">
										<td>Activation</td>
										<td>$30.00 AUD</td>
									</tr>
									<tr class="dark">
										<td>Transport costs</td>
										<td>$25.00 AUD</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="one_quarter">
							<table class="center-content textBold">
								<thead>
									<tr>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light">
										<td>Total</td>
									</tr>
									<tr class="dark">
										<td>$520.00 AUD</td>
									</tr>
									<tr class="light center-content">
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="CBBAJELRMMFSJ">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>
					</div>
					<div id="tab-2" class="tab-content clear">
						<div class="full_width">
							<h1 class="emphasise">PC/Mac Player</h1>
						</div>
						<div class="full_width">
							<table>
								<thead>
									<tr>
										<th>Subscription prices for PC/Mac Flash player - LOCKED IN 24 MONTH CONTRACT</span></th>
									</tr>
								</thead>
							</table>
							<table>
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>Subscription for 3 months</td>
										<td>3 x $25.00 AUD</td>
										<td class="textBold">$75.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="H6LT2SFQXDUTG">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 6 months</td>
										<td>6 x $25.00 AUD</td>
										<td class="textBold">$150.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="4DJDA6JXG5ETW">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>Subscription for 12 months</td>
										<td>12 x $25.00 AUD</td>
										<td class="textBold">$300.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="VKL7BY4HWV8P2">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<table>
								<thead>
									<tr>
										<th>Subscription prices for PC/Mac Flash player - NO CONTRACT</span></th>
									</tr>
								</thead>
							</table>
							<table>
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>Subscription for 1 month</td>
										<td>1 x $32.00 AUD</td>
										<td class="textBold">$32.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="EBSZL46HYURQQ">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 3 months</td>
										<td>3 x $29.00 AUD</td>
										<td class="textBold">$87.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="XFY6JHZBXVESL">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>Subscription for 6 months</td>
										<td>6 x $27.00 AUD</td>
										<td class="textBold">$162.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="25F6TLQNPWL3J">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 12 months</td>
										<td>12 x $25.00 AUD</td>
										<td class="textBold">$300.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="P6PGWZ2ZHRBYG">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>						
					</div>
					<div id="tab-3" class="tab-content clear">
						<div class="full_width">
							<h1 class="emphasise">Smartphone</h1>
							<div class="full_width">
								<table>
									<thead>
										<tr>
											<th>Price for users who HAVE A CONTRACT</span></th>
										</tr>
									</thead>
								</table>
								<table>
									<thead>
										<tr>
											<th>Duration</th>
											<th>Price per month</th>
											<th>Total</th>
											<th>Purchase</th>
										</tr>
									</thead>
									<tbody>
										<tr class="light center-content">
											<td>Subscription for 1 month</td>
											<td>1 x $6.50 AUD</td>
											<td class="textBold">$6.50 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="V47MUY2SJ5G9L">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
										<tr class="dark center-content">
											<td>Subscription for 3 months</td>
											<td>3 x $5.66 AUD</td>
											<td class="textBold">$17.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="JA6GMTGTEE95E">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
										<tr class="light center-content">
											<td>Subscription for 6 months</td>
											<td>6 x $5.10 AUD</td>
											<td class="textBold">$31.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="GA2QSEV6MB4E8">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>												
											</td>
										</tr>
										<tr class="dark center-content">
											<td>Subscription for 12 months</td>
											<td>12 x $4.79 AUD</td>
											<td class="textBold">$57.50 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="LV8SR4F5P2ZDQ">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
									</tbody>								
								</table>
							</div>							
							<div class="full_width">
								<table>
									<thead>
										<tr>
											<th>Price for users who DO NOT HAVE A CONTRACT</span></th>
										</tr>
									</thead>
								</table>
								<table>
									<thead>
										<tr>
											<th>Duration</th>
											<th>Price per month</th>
											<th>Total</th>
											<th>Purchase</th>
										</tr>
									</thead>
									<tbody>
										<tr class="light center-content">
											<td>Subscription for 1 month</td>
											<td>1 x $13.00 AUD</td>
											<td class="textBold">$13.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="68EEMB34ELHHU">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
										<tr class="dark center-content">
											<td>Subscription for 3 months</td>
											<td>3 x $11.33 AUD</td>
											<td class="textBold">$34.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="5B3EJNDN5CR82">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
										<tr class="light center-content">
											<td>Subscription for 6 months</td>
											<td>6 x $10.33 AUD</td>
											<td class="textBold">$62.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="WP9WUHEXT3J6C">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
										<tr class="dark center-content">
											<td>Subscription for 12 months</td>
											<td>12 x $9.50 AUD</td>
											<td class="textBold">$115.00 AUD</td>
											<td>
												<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="KLVQ8HPWPMQD2">
													<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
													<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
												</form>
											</td>
										</tr>
									</tbody>								
								</table>
							</div>							
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>						
					</div>
					<div id="tab-4" class="tab-content clear">
						<div class="full_width">
							<h1 class="emphasise">Renew Box</h1>
						</div>
						<div class="full_width">
							<table class="center-content">
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>Subscription for 3 months</td>
										<td>3 x $25.00 AUD</td>
										<td class="textBold">$75.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="VJGZ74ABUM52A">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 6 months</td>
										<td>6 x $25.00 AUD</td>
										<td class="textBold">$150.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="VJCUPUTGTHRYN">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>Subscription for 12 months</td>
										<td>12 x $25.00 AUD</td>
										<td class="textBold">$300.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="QL6925AAW7XWQ">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>						
					</div>
					<div id="tab-5" class="tab-content clear">
						<div class="full_width">
							<h1 class="emphasise">Samsung Smart</h1>
						</div>
						<div class="full_width">
							<table>
								<thead>
									<tr>
										<th>Subscription prices for Samsung Smart TV - LOCKED IN 24 MONTH CONTRACT</span></th>
									</tr>
								</thead>
							</table>
							<table>
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>Subscription for 3 months</td>
										<td>3 x $25.00 AUD</td>
										<td class="textBold">$75.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="GBDDUNEDJV6V4">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 6 months</td>
										<td>6 x $25.00 AUD</td>
										<td class="textBold">$150.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="3YF77C3U56Q9C">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>Subscription for 12 months</td>
										<td>12 x $25.00 AUD</td>
										<td class="textBold">$300.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="UEUR46JU4UXR6">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Activation</td>
										<td>One time fee*</td>
										<td class="textBold">$30.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="QTE6VJG9FP6QC">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
						</div>
						<div class="full_width">
							<table>
								<thead>
									<tr>
										<th>Subscription prices for Samsung Smart TV - NO CONTRACT</span></th>
									</tr>
								</thead>
							</table>
							<table>
								<thead>
									<tr>
										<th>Duration</th>
										<th>Price per month</th>
										<th>Total</th>
										<th>Purchase</th>
									</tr>
								</thead>
								<tbody>
									<tr class="light center-content">
										<td>Subscription for 1 month</td>
										<td>1 x $32.00 AUD</td>
										<td class="textBold">$32.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="6BT9AF7YEN3F8">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 3 months</td>
										<td>3 x $29.00 AUD</td>
										<td class="textBold">$87.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="GPPPJLWGSHBXC">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="light center-content">
										<td>Subscription for 6 months</td>
										<td>6 x $27.00 AUD</td>
										<td class="textBold">$162.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="7ZL62RZZVU8VG">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Subscription for 12 months</td>
										<td>12 x $25.00 AUD</td>
										<td class="textBold">$300.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="LSYJAQXMCANVA">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
									<tr class="dark center-content">
										<td>Activation</td>
										<td>One time fee*</td>
										<td class="textBold">$30.00 AUD</td>
										<td>
											<form class="paypalButtonForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="QTE6VJG9FP6QC">
												<input class="paypalButtonImg" type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
												<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
											</form>
										</td>
									</tr>
								</tbody>								
							</table>
							<p>* The activation fee is only needed when you purchase a subscription for the first time.</p>
						</div>
						<div class="full_width">
							<p>
								Global NETTV<br/>
								(ABN) 29 167 916 163<br/>
								Melbourne, Australia<br/><br/>
								<span class="textBold">Payment via bank account</span><br/>
								BSB (Branch number): 063-622<br/>
								Account number: 1076 3995<br/>
								Bank: Commonwealth bank<br/><br/>
								After successful payment please send us a copy of the receipt to <span class="textBold">office@globalnettv.com.au</span><br/>
								With following info:<br/>
								First and Last name:<br/>
								Address:<br/>
								Contact phone:<br/>
								E-mail Address:<br/><br/>
								Or contact Velimir Pavicic on <span class="textBold">+61 416 938 723</span> or at <span class="textBold">office@globalnettv.com.au</span><br/>
							</p>
						</div>						
					</div>
				</div>
			</div>
		</section>
		<section class="clear">
			<div class="full_width">
				<h1>What is NetTV Plus box</h1>
				<p>Net TV Plus set-top box is the new generation device, which allows a simple and fast way to get to the TV channels from your home country. The device is small and very easy to use. By connecting the box to the internet, you meet the only requirement for the operation of our service, through which you can watch over 200 ex-Yugoslavian TV channels.</p>
				<p>In addition to basic functions, viewing TV channels, you can use NetTv Plus Set-Top box to listen to radio and read news from the ex-Yugoslavian region. Our advanced software allows you to create a list of favorite TV channels, then EPG - EPG up to 7 days and most importantly we provide you high quality signal. Net TV Set-Top box is capable of decoding signals in SD and HD quality, full HD 1080p, 720p, and standards of PAL and NTSC.</p>
				<img src="/images/nettv_plus_box.jpg">
				<img src="/images/nettv_plus_box_2.jpg">
				<ul class="list tick">
					<li>Over 200 EX-YU tv channels</li>
					<li>HD channels</li>
					<li>EPG-electronic program guide</li>
					<li>Creating a list of favourite channels</li>
					<li>The most popular radio stations</li>
					<li>Box has small dimensions - 140 mm x 95 mm x 335 mm</li>
					<li>Possibility to connect to any TV with Scart or HDMI cable</li>
					<li>Possibility to connect via LAN</li>
					<li>Automatic updates</li>
				</ul>
			</div>
		</section>
		<!-- ################################################################################################ -->
		<div class="clear"></div>
	</div>
</div>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/footer.php";
include_once($path);
?>