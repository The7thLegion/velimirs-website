<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<title>Global NetTV</title>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
		<link href="/layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
		<link href="layout/styles/ie/ie8.css" rel="stylesheet" type="text/css" media="all">
		<script src="layout/scripts/ie/css3-mediaqueries.min.js"></script>
		<script src="layout/scripts/ie/html5shiv.min.js"></script>
		<![endif]-->
	</head>

	<body class="">
		<div class="wrapper row1">
			<header id="header" class="full_width clear">
				<div class="header-logo">
					<a href="/index.php"><img src="/images/global_nettv_logo.png" alt="Global NetTv Logo"></a>
				</div>
				<hgroup class="header-text">
					<h1><a href="/index.php">Global NetTV</a></h1>
				</hgroup>
				<div id="header-contact">
					<ul class="list none">
						<li><span class="icon-envelope"></span> <a href="mailto:office@globalnettv.com.au">office@globalnettv.com.au</a></li>
					</ul>
				</div>
			</header>
		</div>
		<!-- ################################################################################################ -->
		<div class="wrapper row2 rowNoBorder">
			<nav id="topnav">
				<ul class="clear">
					<li <?php if (stripos($_SERVER['REQUEST_URI'],'index') !== false) {echo 'class="active"';} ?>><a href="/index.php" title="Homepage">Home</a></li>
					<li <?php if (stripos($_SERVER['REQUEST_URI'],'buy') !== false) {echo 'class="active"';} ?>><a href="/buy.php" title="Buy">Buy</a></li>
					<li <?php if (stripos($_SERVER['REQUEST_URI'],'download') !== false) {echo 'class="active"';} ?>><a href="/download.php" title="Download">Download</a></li>
					<!-- <li class="active"><a href="#" title="Associate">Associate</a></li> -->
					<li <?php if (stripos($_SERVER['REQUEST_URI'],'contact') !== false) {echo 'class="active"';} ?>><a href="#contact" title="Contact Us">Contact Us</a></li>
					<li class="freeTrial"><a class="button small green" href="/freetrial.php" title="Homepage">Free TV Trial!</a></li>
				</ul>
			</nav>
		</div>