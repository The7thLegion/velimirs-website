		<!-- Footer -->
		<div class="wrapper row2">
			<div id="footer" class="clear">
				<div class="one_third first">
					<h2 class="footer_title">Site map</h2>
					<nav class="footer_nav">
						<ul class="nospace">
							<li><a href="/index.php">Home</a></li>
							<li><a href="/buy.php">Buy</a></li>
							<li><a href="/download.php">Download</a></li>
							<li><a href="/freetrial.php">Free TV Trial</a></li>
						</ul>
					</nav>
				</div>
				<div class="one_third">
					<h2 class="footer_title">Global NetTV Channels</h2>
					<ul id="ft_gallery" class="nospace spacing clear">
						<li title="Balkan Ex-Yu " class="tooltip footerFlag one_quarter first"><a href="/buy/balkan.php"><img src="/images/exyu_flag.png" alt="Balkan Ex-Yu Flag"></a></li>
						<li title="Albania" class="tooltip footerFlag one_quarter"><a href="/buy/albania.php"><img src="/images/albania_flag.png" alt="Albania Flag"></a></li>
						<li title="Russia" class="tooltip footerFlag one_quarter"><a href="http://www.tvrussialive.net/" target="_blank"><img src="/images/russia_flag.png" alt="Russia Flag"></a></li>
						<li title="Polska" class="tooltip footerFlag one_quarter"><a href="http://www.tvpolskalive.com/" target="_blank"><img src="/images/poland_flag.png" alt="Polska Flag"></a></li>
						<li title="Italia" class="tooltip footerFlag one_quarter first"><a href="http://www.tvitalialive.com/" target="_blank"><img src="/images/italy_flag.png" alt="Italia Flag"></a></li>
						<li title="Turk" class="tooltip footerFlag one_quarter"><a href="http://www.tvturklive.com/" target="_blank"><img src="/images/turkey_flag.png" alt="Turk Flag"></a></li>
						<li title="Spain" class="tooltip footerFlag one_quarter"><a href="http://tvespanalive.com/" target="_blank"><img src="/images/spain_flag.png" alt="Spain Flag"></a></li>
					</ul>
				</div>
				<div class="one_third">
					<div id="contact" name="contact">
						<h2 class="footer_title">Contact Us</h2>
						<?php

						$name = '';
						$email = '';
						$contactNumber = '';
						$comments = '';

						if (isset($_GET['field'])) {
							$field = urldecode($_GET['field']);
							$name = urldecode($_GET['name']);
							$email = urldecode($_GET['email']);
							$contactNumber = urldecode($_GET['contactNumber']);
							$comments = urldecode($_GET['comments']);

							echo '<div class="alert-msg rnd8 error">';

							if ($name == '') {
								echo 'Name cannot be empty<br />';
							} else if ($field == 'name') {
								echo 'Name can only contain letters<br />';
							}

							if ($email == '') {
								echo 'Email cannot be empty<br />';
							} else if ($field == 'email') {
								echo 'You must provide a valid email<br />';
							}

							if ($contactNumber == '') {
								echo 'Contact number cannot be empty<br />';
							} else if ($field == 'contactNumber') {
								echo 'You must provide a valid Contact number<br />';
							}

							if ($comments == '') {
								echo 'Comments cannot be empty<br />';
							} else if ($field == 'comments') {
								echo 'Comments must contain a message with more than 10 characters';
							}

							echo '<a class="close" href="#">X</a></div>';
						}
						?>



						<form id="contactUs" class="rnd5" action="/email.php" method="post">
							<div class="form-input clear">
								<label for="name">Name: <span class="required">*</span><br>
									<input type="text" name="name" id="name" value="<?php echo $name; ?>" size="22">
								</label>
								<label for="email">Email: <span class="required">*</span><br>
									<input type="text" name="email" id="email" value="<?php echo $email; ?>" size="22">
								</label>
								<label for="contactNumber">Contact number: <span class="required">*</span><br>
									<input type="text" name="contactNumber" id="contactNumber" value="<?php echo $contactNumber; ?>" size="22">
								</label>
							</div>
							<div class="form-message">
								<label for="comments">Comments: <span class="required">*</span><br>
								<textarea name="comments" id="comments" cols="25" rows="10" placeholder="Please mention which countries you are interested in."><?php echo $comments; ?></textarea>
							</div>
							<p>
								<input type="submit" value="Submit" class="button small orange">
								&nbsp;
								<input type="reset" value="Reset" class="button small grey resetForm">
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper row4">
			<div id="copyright" class="clear">
				<p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="/index.php">globalnettv.com.au</a></p>
				<p class="fl_right">Website by Jacob Van De Velde using <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
			</div>
		</div>
		<!-- Scripts -->
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
		<script>window.jQuery || document.write('<script src="/layout/scripts/jquery-latest.min.js"><\/script>\
		<script src="/layout/scripts/jquery-ui.min.js"><\/script>')</script>
		<script>jQuery(document).ready(function($){ $('img').removeAttr('width height'); });</script>
		<script src="/layout/scripts/jquery-mobilemenu.min.js"></script>
		<script src="/layout/scripts/custom.js"></script>
	</body>
</html>