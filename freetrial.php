<?php include 'header.php'; ?>
<!-- ################################################################################################ -->
<div class="wrapper row1 fixedIntroHeight">
	<!-- ################################################################################################ -->
	<div id="intro" style="position: relative;">
		<div class="boxholder bannerBox" banner_number="1" style="display: block;">
			<a href="https://en.nettvplus.com/" target="_blank"><img src="/images/banner-1.jpg" alt="Net TV Plus"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="2">
			<a href="https://en.nettvplus.com/" target="_blank"><img src="/images/banner-2.jpg" alt="Net TV Plus"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="3">
			<a href="http://www.tvrussialive.net/" target="_blank"><img src="/images/banner-3.jpg" alt="Russia"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="4">
			<a href="http://www.tvturklive.com/" target="_blank"><img src="/images/banner-4.jpg" alt="Turk"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="5">
			<a href="http://www.tvpolskalive.com/" target="_blank"><img src="/images/banner-5.jpg" alt="Polska"></a>
		</div>
		<div class="boxholder bannerBox" banner_number="6">
			<a href="http://www.tvitalialive.com/" target="_blank">
				<img src="/images/banner-6.jpg" alt="Italia">
			</a>
		</div>
		<div class="banner_select">
			<ul>
				<li class="select_banner_button selected_banner" banner_number="1"></li>
				<li class="select_banner_button" banner_number="2"></li>
				<li class="select_banner_button" banner_number="3"></li>
				<li class="select_banner_button" banner_number="4"></li>
				<li class="select_banner_button" banner_number="5"></li>
				<li class="select_banner_button" banner_number="6"></li>
			</ul>
		</div>
	</div>
	<!-- ################################################################################################ -->
	<div class="clear"></div>
</div>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Details:</h1>
			<div id="freeTrial" class="one_half first">
			<?php

			$name = '';
			$email = '';
			$contactNumber = '';
			$comments = '';

			if (isset($_GET['ftfield'])) {
				$field = urldecode($_GET['ftfield']);
				$name = urldecode($_GET['ftname']);
				$email = urldecode($_GET['ftemail']);
				$contactNumber = urldecode($_GET['ftcontactNumber']);
				$comments = urldecode($_GET['ftcomments']);

				echo '<div class="alert-msg rnd8 error">';

				if ($name == '') {
					echo 'Name cannot be empty<br />';
				} else if ($field == 'name') {
					echo 'Name can only contain letters<br />';
				}

				if ($email == '') {
					echo 'Email cannot be empty<br />';
				} else if ($field == 'email') {
					echo 'You must provide a valid email<br />';
				}

				if ($contactNumber == '') {
					echo 'Contact number cannot be empty<br />';
				} else if ($field == 'contactNumber') {
					echo 'You must provide a valid Contact number<br />';
				}

				echo '<a class="close" href="#">X</a></div>';
			}
			?>
				<form id="contactUs" class="rnd5" action="/emailFreeTrial.php" method="post">
					<div class="form-input clear">
						<label for="name">Name: <span class="required">*</span><br>
							<input type="text" name="ftname" id="name" value="<?php echo $name; ?>" size="22">
						</label>
						<label for="email">Email: <span class="required">*</span><br>
							<input type="text" name="ftemail" id="email" value="<?php echo $email; ?>" size="22">
						</label>
						<label for="contactNumber">Contact number: <span class="required">*</span><br>
							<input type="text" name="ftcontactNumber" id="contactNumber" value="<?php echo $contactNumber; ?>" size="22">
						</label>
						<table class="center-content pointerTable">
							<thead>
								<tr>
									<th colspan="4">Channels interested in</th>
								</tr>
							</thead>
							<tbody>
								<tr class="light">
									<td><label><input type="checkbox" name="Serbia" value="Serbia">Serbia</label></td>
									<td><label><input type="checkbox" name="Bosnia" value="Bosnia">Bosnia</label></td>
									<td><label><input type="checkbox" name="Croatia" value="Croatia">Croatia</label></td>
									<td><label><input type="checkbox" name="Macedonia" value="Macedonia">Macedonia</label></td>
								</tr>
								<tr class="dark">
									<td><label><input type="checkbox" name="Montenegro" value="Montenegro">Montenegro</label></td>
									<td><label><input type="checkbox" name="Slovenia" value="Slovenia">Slovenia</label></td>
									<td><label><input type="checkbox" name="Russia" value="Russia">Russia</label></td>
									<td><label><input type="checkbox" name="Albania" value="Albania">Albania</label></td>
								</tr>
								<tr class="light">
									<td><label><input type="checkbox" name="Polska" value="Polska">Polska</label></td> 
									<td><label><input type="checkbox" name="Italia" value="Italia">Italia</label></td> 
									<td><label><input type="checkbox" name="Turk" value="Turk">Turk</label></td> 
									<td><label><input type="checkbox" name="Spain" value="Spain">Spain</label></td> 
								</tr>
							</tbody>								
						</table>
					</div>
					<div class="form-message">
						<label for="comments">Comments:<br>
						<textarea name="ftcomments" id="comments" cols="25" rows="10"><?php echo $comments; ?></textarea>
					</div>
					<p>
						<input type="submit" value="Submit" class="button small orange">
						&nbsp;
						<input type="reset" value="Reset" class="button small grey resetForm">
					</p>
				</form>
			</div>
			<div class="one_half">
				<div class="freeTrialTagline">
					Sign up for your free trial now!
					<img class="freeTrialImage" src="/images/what_is_tv.png">
				</div>
			</div>
		</section>	
<!-- ################################################################################################ -->
<div class="clear"></div>
	</div>
</div>

<?php include 'footer.php'; ?>
