var banner_rotate_speed = 6000; // Milliseconds or six seconds.
var banner_timer = setInterval(banner_slide, banner_rotate_speed);

jQuery(document).ready(function ($) {

    /* Alert Messages */
	
    $(".alert-msg .close").click(function () {
        $(this).parent().animate({
            "opacity": "0"
        }, 400).slideUp(400);
        return false;
    });

    /* Accordions */

    $(".accordion-title").click(function () {
        $(".accordion-title").removeClass("active");
        $(".accordion-content").slideUp("normal");
        if ($(this).next().is(":hidden") == true) {
            $(this).addClass("active");
            $(this).next().slideDown("normal");
        }
    });
    $(".accordion-content").hide();

    /* Toggles */

    $(".toggle-title").click(function () {
        $(this).toggleClass("active").next().slideToggle("fast");
        return false;
    });

    /* Tabs */

    $(".tab-wrapper").tabs({
        event: "click"
    })

    /* Vertically Centre Text On Images */

    $.fn.flexVerticalCenter = function (onAttribute) {
        return this.each(function () {
            var $this = $(this);
            var attribute = onAttribute || 'margin-top';
            var resizer = function () {
                $this.css(
                attribute, (($this.parent().height() - $this.height()) / 2));
            };
            resizer();
            $(window).resize(resizer);
        });
    };

    // To run the function:
    $('.viewit').flexVerticalCenter();

    $('.resetForm').click(function() {
        $('input[name="name"]').removeAttr('value');
        $('input[name="email"]').removeAttr('value');
        $('input[name="contactNumber"]').removeAttr('value');
        $('textarea[name="comments"]').text('');
    });

    // Image select on the buy section.
    $('.productMenuLink').click(function() {

        var productMenuLink = $(this);
        var changeThisImg = productMenuLink.find('img').attr('src');

        // If img is already the selected one... BAIL!
        if (changeThisImg.indexOf("selected_") >= 0) {
            return false;
        }
        
        // Reset all other images.
        var otherMenuLinks = productMenuLink.parent().siblings();

        $.each(otherMenuLinks, function() {
            var menuLink = $(this);
            var currentImg = menuLink.find('img').attr('src');
            var unselectedImg = currentImg.replace('selected_', '');
            menuLink.find('img').attr('src', unselectedImg);
        });

        // Set this image to selected.
        // /images/ = 8 characters
        var position = 8;
        var conCatWith = 'selected_';
        var selectedImg = [changeThisImg.slice(0, position), conCatWith, changeThisImg.slice(position)].join('');

        // Set the new image.
        productMenuLink.find('img').attr('src', selectedImg);

    });

    jQuery('.select_banner_button').click(function() {
        var banner_number = jQuery(this).attr('banner_number');

        var shown_banner = jQuery('.bannerBox:visible');

        if (shown_banner.attr('banner_number') !== banner_number)
        {
            shown_banner.fadeOut('200');
            jQuery('.bannerBox[banner_number="'+banner_number+'"]').fadeIn('600', function() {
            });

            jQuery('.select_banner_button[banner_number="'+banner_number+'"]').addClass('selected_banner').siblings().removeClass('selected_banner');
        }

        reset_banner_timer();

    });

});

function banner_slide() {
    var shown_banner = jQuery('.bannerBox:visible');
    var first_banner = jQuery('.bannerBox:first');
    var next_element = shown_banner.next();

    var banner_number = shown_banner.attr('banner_number');

    // Sometimes the loading icon will not dissappear. this sould correct it.
    if (jQuery('#dvLoading:visible'))
    {
        jQuery('#dvLoading').hide();
    }

    shown_banner.fadeOut('200');

    if (next_element.hasClass('bannerBox'))
    {
        next_element.fadeIn('600', function() {
        });
        jQuery('.select_banner_button[banner_number="'+next_element.attr('banner_number')+'"]').addClass('selected_banner').siblings().removeClass('selected_banner');
    }
    else
    {
        first_banner.fadeIn('600');
        jQuery('.select_banner_button[banner_number="'+first_banner.attr('banner_number')+'"]').addClass('selected_banner').siblings().removeClass('selected_banner');
    }

    reset_banner_timer();
}

// Calling this with a value of true will increase the wait time for the scroll.
function reset_banner_timer(wait_longer) {

    if (wait_longer === true)
    {
        var banner_rotate_speed = 10000; // Milliseconds
    } else
    {
        var banner_rotate_speed = 6000; // Milliseconds
    }

    clearInterval( banner_timer );
    banner_timer = setInterval(banner_slide, banner_rotate_speed);
}