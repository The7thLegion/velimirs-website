<?php

function died($field) {

	$name = urlencode($_POST['name']);
	$emailFrom = urlencode($_POST['email']);
	$contactNumber = urlencode($_POST['contactNumber']);
	$comments = urlencode($_POST['comments']);

	header("Location:".$_SERVER['HTTP_REFERER']."?field=".$field."&name=".$name."&email=".$emailFrom."&contactNumber=".$contactNumber."&comments=".$comments."#contactUs");
	die();
}

function clean_string($string) {
	$bad = array("content-type","bcc:","to:","cc:","href");
	return str_replace($bad,"",$string);
}

// EDIT THE 2 LINES BELOW AS REQUIRED
$email_to = "office@globalnettv.com.au";
// Debugging email address.
// $email_to = "vandevelde.jacob@gmail.com";
$email_subject = "Global Net TV: Contact Form";

$name = $_POST['name'];
$emailFrom = $_POST['email'];
$contactNumber = $_POST['contactNumber'];
$comments = $_POST['comments'];

$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
$number_exp = "/^[0-9 +]+$/";

if (!preg_match($email_exp,$emailFrom)) {
	died('email');
}

if (!preg_match($number_exp,$contactNumber)) {
	died('contactNumber');
} 

if (strlen($comments) < 10) {
	died('comments');
}

$email_message = '<html><body>';

$email_message .= "Form information<hr /><table style='vertical-align:middle;'>";

$email_message .= "<tr style='height: 30px;'><td><b>Name:</b></td><td>".clean_string($name)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td><b>Email:</b></td><td>".clean_string($emailFrom)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td style='padding-right: 15px;'><b>Contact Number:</b></td><td>".clean_string($contactNumber)."</td></tr>";
$email_message .= "<tr style='height: 30px;'><td><b>Comments:</b></td><td>".clean_string($comments)."</td></tr>";

$email_message .= '</table></html></body>';

$headers = "From: " . strip_tags($_POST['email']) . "\r\n";
$headers .= "Reply-To: ". strip_tags($_POST['email']) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

@mail($email_to, $email_subject, $email_message, $headers);  
header("Location: index.php?success");
die();
