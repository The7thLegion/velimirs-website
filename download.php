<?php 
include 'header.php';
?>
<!-- content -->
<div class="wrapper row3">
	<div id="container">
		<!-- ################################################################################################ -->
		<section class="clear">
			<h1>Download</h1>
			<div class="one_half first">
				<div class="introBlurb">
					<a href="/download/balkan.php">Balkan TV | Ex-Yu TV</a>
				</div>
				<div class="balkanTagline">
					We provide over 200 channels from Serbia, Croatia, Bosnia and more...
				</div>				
				<div class="introFlags">					
					<ul class="introFlagsList">
						<a href="/download/balkan.php">
							<li class="introFlagsItem"><img src="/images/exyu_flag.png" alt="Balkan Ex-Yu Flag"></li>
						</a>
					</ul>
					<a class="button small orange rnd8" href="/download/balkan.php">View downloads</a>
				</div>
			</div>
			<div class="one_half">
				<div class="introBlurb">
					<a href="http://player.tvalbanialive.com/" target="_blank">Albania | IPTV Iliria</a>
				</div>
				<div class="balkanTagline">
					We provide over 50 channels in Albanian language on our IPTV Iliria platform
				</div>				
				<div class="introFlags">					
					<ul class="introFlagsList">
						<a href="http://player.tvalbanialive.com/" target="_blank">
							<li class="introFlagsItem firstFlag"><img src="/images/albania_flag.png" alt="Albania Flag"></li>
						</a>
					</ul>
					<a class="button small orange rnd8" href="http://player.tvalbanialive.com/" target="_blank">Open player</a>
				</div>
			</div>
		</section>	
<!-- ################################################################################################ -->
<div class="clear"></div>
</div>
</div>
<?php include 'footer.php'; ?>